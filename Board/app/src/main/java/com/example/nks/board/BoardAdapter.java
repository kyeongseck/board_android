package com.example.nks.board;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by nks on 2016-12-11.
 */

public class BoardAdapter extends BaseAdapter {
    ArrayList<BoardVo> boards = new ArrayList<BoardVo>();
    Context mContext;

    public BoardAdapter (Context context){
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return boards.size();
    }

    @Override
    public Object getItem(int i) {
        return boards.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View v, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_board_list, null);

        TextView tvName = (TextView)view.findViewById(R.id.name);
        TextView tvContent = (TextView)view.findViewById(R.id.content);

        return view;
    }
}
