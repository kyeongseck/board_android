package com.example.nks.board;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    Button button1;
    Button button2;
    ListView listView;
    BoardAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button1 = (Button) findViewById(R.id.button);
        listView = (ListView) findViewById(R.id.board_list);

        button1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(MainActivity.this,WriteActivity.class);
                startActivity(intent);
            }
        });

        adapter = new BoardAdapter(this);
        listView.setAdapter(adapter);

        BoardVo vo = new BoardVo();

        vo.setName("이름");
        vo.setContent("내용");

        adapter.boards.add(vo);



        /*try {
            String url = "http://default-environment.jwgwrggxca.ap-northeast-2.elasticbeanstalk.com/read";

            JsonArrayRequest jsArrRequest = new JsonArrayRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {
                            try {
                                for (int i = 0; i < response.length(); i++) {
                                    JSONObject obj = (JSONObject) response.get(i);

                                    String name = obj.getString("name");
                                    String content = obj.getString("content");

                                    Log.i("NKS", name + "/" + content);
                                }
                            } catch (Exception e) {

                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //error
                        }
                    });

            RequestQueue queue = Volley.newRequestQueue(this);
            queue.add(jsArrRequest);

        } catch (Exception e) {

        }*/

    }
}
