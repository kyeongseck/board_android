package com.example.nks.board;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class WriteActivity extends AppCompatActivity {

    Button button6;
    Button button7;

   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write);

       button6 = (Button) findViewById(R.id.button6);
       button7 = (Button) findViewById(R.id.button7);

       button6.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

           }
       });

       button7.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent intent = new Intent(WriteActivity.this,MainActivity.class);
               startActivity(intent);
           }
       });
    }
}
